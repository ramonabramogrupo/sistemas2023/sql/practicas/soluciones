![ramon](imgs/ramon.jpeg)

<hr>

![sql](/imgs/todosql.jpg)

![logo](imgs/rpeque.png)Hola 🖐 soy Ramon Abramo 

> **Estas son las soluciones de cada una de las practicas de la unidad de SQL**

[[_TOC_]]

![soluciones](imgs/solucionesSql.png)

# Practicas de LDD
- practica 1 .- Implementar Coches y Clientes
- practica 2 .- Implementar Coches y Clientes
- practica 3 .- Implementar Coches, clientes y poblacion
- practica 4 .- Implementar Empleado, supervisa, dependiente, proyectos, departamentos
- practica 5 .- Implementar Alumnos, Delegados, modulos, profesores
- practica 6 .- Implementar Cliente, Coche, Compra, Revision. Ejercicios ALTER TABLE
- practica 7 .- Implementar Alumno, Realiza, Practica, FechaP
- practica 8 .- Implementar Cliente, compra, producto, proveedor. Otro Camioneros, paquetes

# Practicas de LMD

- practica 9 .- Consultas de seleccion de una sola tabla (ciclistas)
- practica 9A .- Consultas de seleccion de un sola tabla (empresas). Orientada con funciones y LIKE
- practica 10 .- Consultas de totales (ciclistas)
- practica 11 .- Consultas de seleccion de una sola tabla y de totales (ciclistas). Repaso
- practica 12 .- Consultas de combinacion internas (ciclistas)
- practica 13 .-  Consultas de seleccion repaso (empiezo a utilizar clausulas conjuntos) (emple,depart)
- **[Practica14.-](PRACTICA14/)** Consultas de seleccion de una sola tabla (tienda)

![sql](imgs/sql.jpg)
