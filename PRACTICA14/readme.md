![ramon](../imgs/profe.png)
<hr>
![portada](../imgs/1.jpg)
![portada](../imgs/2.jpg)

[[_TOC_]]

# Practica 14

## Consultas de seleccion de una tabla

En esta práctica, nos sumergiremos en el corazón de SQL: las consultas de selección. SQL, o Lenguaje de Consulta Estructurado, es el estándar de facto para interactuar con bases de datos relacionales. A través de una serie de ejercicios prácticos, exploraremos cómo la cláusula SELECT nos permite acceder a los datos almacenados en una tabla, seleccionando específicamente los registros y campos que necesitamos.

Comenzaremos con consultas simples, seleccionando todas las columnas de una tabla con SELECT * FROM nombre_tabla. Gradualmente, introduciremos cláusulas adicionales como WHERE para filtrar registros, ORDER BY para ordenar los resultados, GROUP BY para agrupar datos similares y HAVING para filtrar grupos. También veremos cómo las funciones de agregación como COUNT, SUM, MAX, y MIN pueden proporcionar estadísticas resumidas de nuestros datos.

Cada cláusula y función se explicará con ejemplos detallados, asegurando que al final de esta práctica, tengas una comprensión sólida de cómo realizar consultas de selección efectivas y eficientes en una sola tabla.

Prepárate para convertirte en un maestro de las consultas de selección y descubrir el poder de SQL para manipular y analizar datos.

## Base de datos tienda
La base de datos con la que vamos a trabajar contiene dos tablas de Fabricantes y Productos.

### Esquema de relaciones

La base de datos con la que vamos a trabajar dispone del siguiente esquema de relaciones.

![esquema](../imgs/esquema_relaciones.png)

### Datos

El contenido de las tablas es el siguiente.

**Fabricante**

|codigo|nombre|
|------|------|
|1|Asus|
|2|Lenovo|
|3|Hewlett-Packard|
|4|Samsung|
|5|Seagate|
|6|Crucial|
|7|Gigabyte|
|8|Huawei|
|9|Xiaomi

**Producto**

|codigo|nombre|precio|codigo_fabricante|
|------|------|------|-----------------|
|1|Disco duro SATA3 1TB|86.99|5|
|2|Memoria RAM DDR4 8GB|120|6|
|3|Disco SSD 1 TB|150.99|4|
|4|GeForce GTX 1050Ti|185|7|
|5|GeForce GTX 1080 Xtreme|755|6|
|6|Monitor 24 LED Full HD|202|1|
|7|Monitor 27 LED Full HD|245.99|1|
|8|Portátil Yoga 520|559|2|
|9|Portátil Ideapd 320|444|2|
|10|Impresora HP Deskjet 3720|59.99|3|
|11|Impresora HP Laserjet Pro M26nw|180|3

### Codigo SQL

Os coloco el codigo SQL para crear la base de datos tienda que utilizamos.

La teneis disponible en el propio repositorio.

~~~sql
SET NAMES 'utf8';

DROP DATABASE IF EXISTS tienda;

CREATE DATABASE IF NOT EXISTS tienda
CHARACTER SET utf8mb4
COLLATE utf8mb4_0900_ai_ci;

--
-- Set default database
--
USE tienda;

--
-- Create table `fabricante`
--
CREATE TABLE IF NOT EXISTS fabricante (
  codigo int UNSIGNED NOT NULL AUTO_INCREMENT,
  nombre varchar(100) NOT NULL,
  PRIMARY KEY (codigo)
)
ENGINE = INNODB,
AUTO_INCREMENT = 11,
AVG_ROW_LENGTH = 1820,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_0900_ai_ci,
ROW_FORMAT = DYNAMIC;

--
-- Create table `producto`
--
CREATE TABLE IF NOT EXISTS producto (
  codigo int UNSIGNED NOT NULL AUTO_INCREMENT,
  nombre varchar(100) NOT NULL,
  precio double NOT NULL,
  codigo_fabricante int UNSIGNED NOT NULL,
  PRIMARY KEY (codigo)
)
ENGINE = INNODB,
AUTO_INCREMENT = 14,
AVG_ROW_LENGTH = 1489,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_0900_ai_ci,
ROW_FORMAT = DYNAMIC;

--
-- Create foreign key
--
ALTER TABLE producto
ADD CONSTRAINT producto_ibfk_1 FOREIGN KEY (codigo_fabricante)
REFERENCES fabricante (codigo);

-- 
-- Dumping data for table fabricante
--
INSERT INTO fabricante VALUES
(1, 'Asus'),
(2, 'Lenovo'),
(3, 'Hewlett-Packard'),
(4, 'Samsung'),
(5, 'Seagate'),
(6, 'Crucial'),
(7, 'Gigabyte'),
(8, 'Huawei'),
(9, 'Xiaomi');

-- 
-- Dumping data for table producto
--
INSERT INTO producto VALUES
(1, 'Disco duro SATA3 1TB', 86.99, 5),
(2, 'Memoria RAM DDR4 8GB', 120, 6),
(3, 'Disco SSD 1 TB', 150.99, 4),
(4, 'GeForce GTX 1050Ti', 185, 7),
(5, 'GeForce GTX 1080 Xtreme', 755, 6),
(6, 'Monitor 24 LED Full HD', 202, 1),
(7, 'Monitor 27 LED Full HD', 245.99, 1),
(8, 'Portátil Yoga 520', 559, 2),
(9, 'Portátil Ideapd 320', 444, 2),
(10, 'Impresora HP Deskjet 3720', 59.99, 3),
(11, 'Impresora HP Laserjet Pro M26nw', 180, 3);
~~~

## Enunciados

![enunciados](../imgs/ENUNCIADOS.png)

- Ejercicio 1.- Lista el nombre de todos los productos que hay en la tabla producto.
- Ejercicio 2.- Lista los nombres y los precios de todos los productos de la tabla producto.
- Ejercicio 3 .- Muestra los productos ordenados por nombre de forma ascendente y despues por precio de forma descendente.
- Ejercicio 4 .- Lista el nombre de los productos y el precio y un campo nuevo que nos muestre el codigo del producto seguido de un guion y despues el nombre del producto (1-Disco Duro). Este nuevo campo denominarlo Referencia
- Ejercicio 5.- Lista el nombre de los productos, el precio y el precio sin decimales. Para quitar los decimales quiero truncar. Utiliza los siguientes alias para las columnas: nombre de producto, precio, precio truncado.
- Ejercicio 6.- Lista los nombres y los precios de todos los productos de la tabla producto, convirtiendo los nombres a mayúscula.
- Ejercicio 7.- Lista el nombre de todos los fabricantes y en otra columna obtenga en mayúsculas los dos primeros caracteres del nombre del fabricante.
  - opcion 1 : utilizando SUBSTRING
  - opcion 2 : utilizando SUBSTRING_INDEX
  - opcion 3 : utilizando LEFT
- Ejercicio 8.- Lista los nombres y los precios de todos los productos de la tabla producto, redondeando el valor del precio a 1 decimal.

<br>

## Soluciones

<br>

![soluciones](../imgs/soluciones.jpg)

<br>


### **Ejercicio 1.- Lista el nombre de todos los productos que hay en la tabla producto.**
---

<br>

> #### **💾 Solucion**
> 📝 SQL

```sql
SELECT DISTINCT p.nombre FROM producto p;
```

<br>

> #### **🖨 Salida**

|nombre|
|------|
|Disco duro SATA3 1TB|
|Memoria RAM DDR4 8GB|
|Disco SSD 1 TB|
|GeForce GTX 1050Ti|
|GeForce GTX 1080 Xtreme|
|Monitor 24 LED Full HD|
|Monitor 27 LED Full HD|
|Portátil Yoga 520|
|Portátil Ideapd 320|
|Impresora HP Deskjet 3720|
|Impresora HP Laserjet Pro M26nw

<br>

### **Ejercicio 2.- Lista los nombres y los precios de todos los productos de la tabla producto.**
---

<br>

> #### **💾 Solucion**
> 📝 SQL

```sql
SELECT DISTINCT p.nombre, p.precio FROM producto p;
```

<br>

> #### **🖨 Salida**

|nombre|precio|
|------|------|
|Disco duro SATA3 1TB|86.99|
|Memoria RAM DDR4 8GB|120|
|Disco SSD 1 TB|150.99|
|GeForce GTX 1050Ti|185|
|GeForce GTX 1080 Xtreme|755|
|Monitor 24 LED Full HD|202|
|Monitor 27 LED Full HD|245.99|
|Portátil Yoga 520|559|
|Portátil Ideapd 320|444|
|Impresora HP Deskjet 3720|59.99|
|Impresora HP Laserjet Pro M26nw|180

<br>

### **Ejercicio 3 .- Muestra los productos ordenados por nombre de forma ascendente y despues por precio de forma descendente.**
---

<br>

> #### **💾 Solucion**
> 📝 SQL

```sql
SELECT * FROM producto p ORDER BY p.nombre ASC, p.precio DESC;
```

<br>

> #### **🖨 Salida**

|codigo|nombre|precio|codigo_fabricante|
|------|------|------|-----------------|
|1|Disco duro SATA3 1TB|86.99|5|
|3|Disco SSD 1 TB|150.99|4|
|4|GeForce GTX 1050Ti|185|7|
|5|GeForce GTX 1080 Xtreme|755|6|
|10|Impresora HP Deskjet 3720|59.99|3|
|11|Impresora HP Laserjet Pro M26nw|180|3|
|2|Memoria RAM DDR4 8GB|120|6|
|6|Monitor 24 LED Full HD|202|1|
|7|Monitor 27 LED Full HD|245.99|1|
|9|Portátil Ideapd 320|444|2|
|8|Portátil Yoga 520|559|2

<br>

### **Ejercicio 4 .- Lista el nombre de los productos y el precio y un campo nuevo que nos muestre el codigo del producto seguido de un guion y despues el nombre del producto (1-Disco Duro). Este nuevo campo denominarlo Referencia**

<br>

> #### **💾 Solucion**
> 📝 SQL

```sql
SELECT p.nombre, p.precio, CONCAT(p.codigo, '-', p.nombre) Referencia FROM producto p;
```

<br>

> #### **🖨 Salida**

|nombre|precio|Referencia|
|------|------|----------|
|Disco duro SATA3 1TB|86.99|1-Disco duro SATA3 1TB|
|Memoria RAM DDR4 8GB|120|2-Memoria RAM DDR4 8GB|
|Disco SSD 1 TB|150.99|3-Disco SSD 1 TB|
|GeForce GTX 1050Ti|185|4-GeForce GTX 1050Ti|
|GeForce GTX 1080 Xtreme|755|5-GeForce GTX 1080 Xtreme|
|Monitor 24 LED Full HD|202|6-Monitor 24 LED Full HD|
|Monitor 27 LED Full HD|245.99|7-Monitor 27 LED Full HD|
|Portátil Yoga 520|559|8-Portátil Yoga 520|
|Portátil Ideapd 320|444|9-Portátil Ideapd 320|
|Impresora HP Deskjet 3720|59.99|10-Impresora HP Deskjet 3720|
|Impresora HP Laserjet Pro M26nw|180|11-Impresora HP Laserjet Pro M26nw

<br>

### **Ejercicio 5.- Lista el nombre de los productos, el precio y el precio sin decimales. Para quitar los decimales quiero truncar. Utiliza los siguientes alias para las columnas: nombre de producto, precio, precio truncado.**

<br>

> #### **💾 Solucion**
> 📝 SQL

```sql
SELECT 
    p.nombre `nombre de producto`,
    p.precio,
    TRUNCATE(p.precio,0) `precio truncado`
  FROM producto p;
```

<br>

> #### **🖨 Salida**

|nombre de producto|precio|precio truncado|
|------------------|------|---------------|
|Disco duro SATA3 1TB|86.99|86|
|Memoria RAM DDR4 8GB|120|120|
|Disco SSD 1 TB|150.99|150|
|GeForce GTX 1050Ti|185|185|
|GeForce GTX 1080 Xtreme|755|755|
|Monitor 24 LED Full HD|202|202|
|Monitor 27 LED Full HD|245.99|245|
|Portátil Yoga 520|559|559|
|Portátil Ideapd 320|444|444|
|Impresora HP Deskjet 3720|59.99|59|
|Impresora HP Laserjet Pro M26nw|180|180

<br>

### **Ejercicio 6.- Lista los nombres y los precios de todos los productos de la tabla producto, convirtiendo los nombres a mayúscula.**

<br>

> #### **💾 Solucion**
> 📝 SQL

```sql
SELECT p.precio, UPPER(p.nombre) FROM producto p;
```

<br>

> #### **🖨 Salida**

|precio|UPPER(p.nombre)|
|------|---------------|
|86.99|DISCO DURO SATA3 1TB|
|120|MEMORIA RAM DDR4 8GB|
|150.99|DISCO SSD 1 TB|
|185|GEFORCE GTX 1050TI|
|755|GEFORCE GTX 1080 XTREME|
|202|MONITOR 24 LED FULL HD|
|245.99|MONITOR 27 LED FULL HD|
|559|PORTÁTIL YOGA 520|
|444|PORTÁTIL IDEAPD 320|
|59.99|IMPRESORA HP DESKJET 3720|
|180|IMPRESORA HP LASERJET PRO M26NW

<br>

### **Ejercicio 7.- Lista el nombre de todos los fabricantes y en otra columna obtenga en mayúsculas los dos primeros caracteres del nombre del fabricante.**

<br>

> #### **💾 Solucion**
> 📝 SQL

<br>

##### **Solucion Funcion SUBSTRING**
| SUBSTRING |
| --- | 
| La función SUBSTRING en MySQL se utiliza para extraer una subcadena de una cadena. |
| La sintaxis básica es la siguiente: <br><br> ` SUBSTRING(cadena, inicio, longitud)` <br><br> |
|Donde: <br> - cadena es la cadena de la que quieres extraer la subcadena. <br> - inicio es la posición inicial desde la que quieres extraer la subcadena. La primera posición es 1.<br>- longitud es el número de caracteres que quieres extraer.|

```sql
SELECT p.nombre, UPPER(SUBSTRING(p.nombre, 1, 2)) FROM fabricante p ;
```

<br>

> ##### **Solucion Funcion SUBSTRING_INDEX**
| SUBSTRING_INDEX |
| --- | 
|La función SUBSTRING_INDEX en MySQL se utiliza para devolver una subcadena de una cadena antes de un número especificado de ocurrencias de un delimitador. |
|La sintaxis básica es la siguiente: <br><br>`SUBSTRING_INDEX(cadena, delimitador, número)` <br><br>| 
|Donde:<br> - cadena es la cadena de la que quieres extraer la subcadena. <br>- delimitador es el carácter o caracteres que separan las subcadenas dentro de la cadena.<br>- número es el número de veces que ocurre el delimitador en la cadena antes de que se extraiga la subcadena.|

```sql
SELECT p.nombre, UPPER(SUBSTRING_INDEX(p.nombre, ' ', 1)) FROM fabricante p ;
```

<br>

> ##### **Solucion Funcion LEFT**
| LEFT |
| --- | 
|La función LEFT en MySQL se utiliza para extraer un número específico de caracteres desde el inicio (izquierda) de una cadena.| 
|La sintaxis básica es la siguiente:<br><br> `LEFT(cadena, número)` <br><br> |
|Donde:<ul><li>cadena es la cadena de la que quieres extraer caracteres.</li><li>número es el número de caracteres que quieres extraer desde el inicio de la cadena.</li><ul>|

```sql
SELECT p.nombre, UPPER(LEFT(p.nombre, 2)) FROM fabricante p ;
```

<br>

> #### **🖨 Salida**

|nombre|UPPER(SUBSTRING(p.nombre, 1, 2))|
|------|--------------------------------|
|Asus|AS|
|Lenovo|LE|
|Hewlett-Packard|HE|
|Samsung|SA|
|Seagate|SE|
|Crucial|CR|
|Gigabyte|GI|
|Huawei|HU|
|Xiaomi|XI

<br>

### **Ejercicio 8.- Lista los nombres y los precios de todos los productos de la tabla producto, redondeando el valor del precio a 1 decimal.**

<br>

![AYUDA](../imgs/AYUDA1.png)

<table>
<thead>
<tr>
<th> Funcion Round</th>
</tr>
</thead>
<tbody>
<tr>
<td>
La función ROUND en MySQL se utiliza para redondear un número a un número específico de decimales.

La sintaxis básica es la siguiente: 

```sql
ROUND(numero, decimales) 
```
Donde:

- número es el número que quieres redondear.
- decimales es el número de decimales a los que quieres redondear el número.

</td>
</tr>
<tr>
<td>
Veamos un ejemplo:

```sql
SELECT ROUND(123.456, 2);
```

En este caso, la función ROUND devolverá el número 123.46, ya que 123.456 redondeado a 2 decimales es 123.46.

</td>
</tr>
</tbody>
</table>

<br>

> #### **💾 Solucion**
> 📝 SQL

```sql
SELECT 
    p.nombre,
    ROUND(p.precio, 1) precioRedondeado
  FROM producto p ;
```
<br>

> #### **🖨 Salida**

|nombre|precioRedondeado|
|------|----------------|
|Disco duro SATA3 1TB|87|
|Memoria RAM DDR4 8GB|120|
|Disco SSD 1 TB|151|
|GeForce GTX 1050Ti|185|
|GeForce GTX 1080 Xtreme|755|
|Monitor 24 LED Full HD|202|
|Monitor 27 LED Full HD|246|
|Portátil Yoga 520|559|
|Portátil Ideapd 320|444|
|Impresora HP Deskjet 3720|60|
|Impresora HP Laserjet Pro M26nw|180


### **Ejercicio 9.- Lista los nombres y los precios de los productos de la tabla producto cuyo precio se encuentre entre 500 y 700 .**


<br>

> #### **💾 Solucion**
> 📝 SQL

<br>

##### Solucion 1 - BETWEEN

```sql
SELECT 
    p.nombre,
    p.precio
  FROM producto p
  WHERE p.precio BETWEEN 500 AND 700 ;
```

<br>

##### Solucion 2 - AND

```sql
SELECT 
    p.nombre,
    p.precio
  FROM producto p
  WHERE p.precio >= 500 AND p.precio <= 700 ;
```

<br>

##### Solucion 3 - INTERSECT

```sql
SELECT 
    p.nombre,
    p.precio
  FROM producto p
  WHERE p.precio >= 500
  INTERSECT
  SELECT 
    p.nombre,
    p.precio
  FROM producto p
  WHERE p.precio <= 700 ;
```

<br>

> #### **🖨 Salida**

|nombre|precio|
|------|------|
|Portátil Yoga 520|559

<br>

![portada](../imgs/profesorramon.jpg){width=100%}