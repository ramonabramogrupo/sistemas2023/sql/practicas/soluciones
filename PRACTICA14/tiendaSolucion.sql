﻿USE tienda;

# Ejercicio 1.- Lista el nombre de todos los productos que hay en la tabla producto.

SELECT DISTINCT p.nombre FROM producto p;

# Ejercicio 2.- Lista los nombres y los precios de todos los productos de la tabla producto.
SELECT DISTINCT p.nombre, p.precio FROM producto p;

# Ejercicio 3 .- Muestra los productos ordenados por nombre de forma ascendente y despues por precio de forma descendente.

SELECT * FROM producto p ORDER BY p.nombre ASC, p.precio DESC;

# Ejercicio 4 .- Lista el nombre de los productos y el precio y un campo nuevo que nos muestre el codigo del producto seguido de un guion y despues el nombre del producto (1-Disco Duro). Este nuevo campo denominarlo Referencia

SELECT p.nombre, p.precio, CONCAT(p.codigo, '-', p.nombre) Referencia FROM producto p;

# Ejercicio 5.- Lista el nombre de los productos, el precio y el precio sin decimales. Para quitar los decimales quiero truncar. Utiliza los siguientes alias para las columnas: nombre de producto, precio, precio truncado.

SELECT 
    p.nombre `nombre de producto`,
    p.precio,
    TRUNCATE(p.precio,0) `precio truncado`
  FROM producto p;


# Ejercicio 6.- Lista los nombres y los precios de todos los productos de la tabla producto, convirtiendo los nombres a mayúscula.

SELECT p.precio, UPPER(p.nombre) FROM producto p;

# Ejercicio 7.- Lista el nombre de todos los fabricantes y en otra columna obtenga en mayúsculas los dos primeros caracteres del nombre del fabricante.

-- opcion 1
-- utilizando SUBSTRING
SELECT p.nombre, UPPER(SUBSTRING(p.nombre, 1, 2)) FROM fabricante p;

-- opcion 2
-- utilizando SUBSTRING_INDEX
SELECT p.nombre, UPPER(SUBSTRING_INDEX(p.nombre, ' ', 1)) FROM fabricante p;

-- opcion 3
-- utilizando LEFT
SELECT p.nombre, UPPER(LEFT(p.nombre, 2)) FROM fabricante p;

# Ejercicio 8.- Lista los nombres y los precios de todos los productos de la tabla producto, redondeando el valor del precio a 1 decimal.

SELECT 
    p.nombre,
    ROUND(p.precio, 1) precioRedondeado
  FROM producto p;


# Ejercicio 9.- Lista los nombres y los precios de los productos de la tabla producto cuyo precio se encuentre entre 500 y 700 .

-- opcion 1 
-- utilizando between 
SELECT 
    p.nombre,
    p.precio
  FROM producto p
  WHERE p.precio BETWEEN 500 AND 700;

-- opcion 2
-- utilizando and
SELECT 
    p.nombre,
    p.precio
  FROM producto p
  WHERE p.precio >= 500 AND p.precio <= 700;


  -- opcion 3
  -- utilizando el operador de conjuntos intersect 

SELECT 
    p.nombre,
    p.precio
  FROM producto p
  WHERE p.precio >= 500
  INTERSECT
  SELECT 
    p.nombre,
    p.precio
  FROM producto p
  WHERE p.precio <= 700;

# Ejercicio 10.- Lista el codigo del fabricante de los fabricantes que tienen productos en la tabla producto.


# Ejercicio 11.- Lista el codigo de los fabricantes que tienen productos en la tabla producto cuyos precios son menores de 100 € o mayores de 400€

-- opcion 1
-- Realizarlo con OR

SELECT DISTINCT p.codigo_fabricante FROM producto p WHERE p.precio < 100 OR p.precio > 400;

-- opcion 2 
-- Realizarlo con el operador de conjuntos UNION

SELECT DISTINCT p.codigo_fabricante FROM producto p WHERE p.precio < 100 
  UNION
SELECT DISTINCT p.codigo_fabricante FROM producto p WHERE p.precio > 400;


# Ejercicio 12.- Devuelve una lista con las 5 primeras filas de la tabla producto ordenados por precio de forma descendente

SELECT * FROM producto p ORDER BY p.precio DESC LIMIT 5;

# Ejercicio 13.-Devuelve una lista con 2 filas a partir de la cuarta fila de la tabla fabricante ordenados por codigo de forma ascendente. Es decir quiero la cuarta y la quinta fila.

SELECT * FROM fabricante p ORDER BY p.codigo ASC LIMIT 3,2;


# Ejercicio 14.- Lista el nombre y el precio del producto más barato. (Utilice solamente las cláusulas ORDER BY y LIMIT)

SELECT * FROM producto p ORDER BY p.precio ASC LIMIT 1;

# Ejercicio 15.- Lista el nombre y el precio del producto más caro. (Utilice solamente las cláusulas ORDER BY y LIMIT)

SELECT * FROM producto p ORDER BY p.precio DESC LIMIT 1;

# Ejercicio 16.- Lista el nombre de los productos que tienen un precio mayor o igual a 400€.

SELECT p.nombre FROM producto p WHERE p.precio >= 400;

# Ejercicio 17.-Lista el nombre de los productos que no tienen un precio mayor o igual a 400€.

-- opcion 1
-- sin utilizar NOT
SELECT p.nombre FROM producto p WHERE p.precio < 400;

-- opcion 2
-- utilizando NOT

SELECT p.nombre FROM producto p WHERE NOT p.precio >= 400;

# Ejercicio 18.- Lista todos los productos que no tengan un precio entre 80€ y 300€ (no me valen los que valgan 80 o 300)

-- opcion 1
-- sin utilizar NOT
SELECT * FROM producto p WHERE p.precio < 80 OR p.precio > 300;

-- opcion 2
-- utilizando NOT
SELECT * FROM proyecto p WHERE NOT p.precio BETWEEN 80 AND 300;

# Ejercicio 19.- Lista todos los productos que tengan un precio mayor que 200€ y que el identificador de fabricante sea igual a 6.

SELECT * FROM producto p WHERE p.precio > 200 AND p.codigo_fabricante = 6;

# Ejercicio 20.- Lista todos los productos donde su el codigo del fabricante sea 2, 3 o 6. 

-- opcion 1
-- sin utilizar el operador IN
SELECT * FROM producto p WHERE p.codigo_fabricante = 2 OR p.codigo_fabricante = 3 OR p.codigo_fabricante = 6;

-- opcion 2
-- utilizando el operador IN
SELECT * FROM producto p WHERE p.codigo_fabricante IN (2,3,6);

-- opcion 3
-- utilizando el operador de conjuntos UNION

SELECT * FROM producto p WHERE p.codigo_fabricante = 2
  UNION
SELECT * FROM producto p WHERE p.codigo_fabricante = 3
  UNION
SELECT * FROM producto p WHERE p.codigo_fabricante = 6;

# Ejercicio 21.- Lista los nombres de los fabricantes cuyo nombre empiece por la letra A o por la letra S.

-- opcion 1
-- utilizando comodines

SELECT p.nombre FROM fabricante p WHERE p.nombre LIKE 'A%' OR p.nombre LIKE 'S%';

-- opcion 2
-- utilizando SUBSTRING
SELECT p.nombre FROM fabricante p WHERE SUBSTRING(p.nombre, 1, 1) IN ('A','S');

-- opcion 3
-- utilizando LEFT
SELECT p.nombre FROM fabricante p WHERE LEFT(p.nombre, 1) IN ('A','S');

# Ejercicio 22.- Lista los nombres de los fabricantes cuyo nombre contenga el carácter X.

-- opcion 1
-- utilizando comodines
SELECT p.nombre FROM fabricante p WHERE p.nombre LIKE '%X%';

-- opcion 2
-- utilizando LOCATE
SELECT p.nombre FROM fabricante p WHERE LOCATE('X',p.nombre) > 0;

-- opcion 3
-- utilizando SUBSTRING_INDEX
SELECT p.nombre FROM fabricante p WHERE SUBSTRING_INDEX(LOWER(p.nombre), 'x', 1) <> p.nombre;

# Ejercicio 23.- Lista los nombres de los productos de los cuales no conocemos el precio

SELECT p.nombre FROM producto p WHERE p.precio IS NULL;


# Ejercicio 24.- Listar el nombre de todos los productos junto con un campo calculado nuevo denominado Tipo. Este campo tipo debe tener el valor BARATO si el precio del producto es menor que 200 y CARO en caso contrario

SELECT p.nombre, IF(p.precio < 200, 'BARATO', 'CARO') FROM producto p;


# Ejercicio 25.- Listar el nombre de todos los productos junto con un campo calculado nuevo denominado Tipo. Este campo tipo debe tener el valor BARATO si el precio del producto es menor que 200 y NORMAL si el precio esta entre 200 y 400 y CARO en caso de ser mayor a 400

SELECT p.nombre, IF(p.precio < 200, 'BARATO', IF(p.precio BETWEEN 200 AND 400, 'NORMAL', 'CARO')) FROM producto p;

# Ejercicio 26.- Listar el nombre de los productos y los precios. El precio de los productos debe estar formateado de tal forma que salgan 5 digitos en la parte entera y dos digitos en la parte DECIMAL

SELECT p.nombre, IF(LENGTH(round(p.precio,2)) < 10, LPAD(FORMAT(p.precio,2),10,'0'),format(p.precio,2)) FROM producto p;


